module.exports = function (mongoose) {

  var Schema = mongoose.Schema;

  var StepSchema = new Schema(
    {
      type         : String,
      title        : String,
      content      : String,
      continueAfter: Number,
      continueWhen : String,
      showAt       : String,
      showSkip     : Boolean,
      align        : String,
      help         : Boolean
    }
  );

  var RolesSchema = new Schema({
    name: String
  });

  var PageSchema = new Schema({
    name       : String,
    description: String,
    steps: [StepSchema]
  });

  // Mongoose model object
  var GuideSchema = new Schema({
    name       : String,
    description: String,
    nextGuides : String,
    startUrl   : String,
    pages      : [PageSchema],
    roles      : []
  });

  return mongoose.model('Guide', GuideSchema, 'guias');
}