module.exports = function (mongoose) {

  var Schema = mongoose.Schema;

  var UserSchema = new Schema({
    username: { type: String, required: true, index: { unique: true } },
    hash    : { type: String, required: true },
    salt    : { type: String, required: true}
  });

  UserSchema.methods.validatePassword = function (password, crypto) {
    sha2sum = crypto.createHash('sha256');
    sha2sum.update(password + this.salt);
    var originalDigest = sha2sum.digest('hex');

    if (this.hash == originalDigest)
      return true;
    return false;
  }


  return mongoose.model('User', UserSchema, 'users');
}