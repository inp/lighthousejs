if (!global.hasOwnProperty('db')) {

  var mongoose = require('mongoose');
  var config = require('../config.js');

  // the application is executed on the local machine ...
  mongoose.connect('mongodb://' + config.dbUser + (config.dbUser ? ':' : '') + config.dbPassword + (config.dbUser ? '@' : '') + config.dbHost + '/' + config.dbName);
  var db = mongoose.connection;
  db.on('error', console.error.bind(console, 'connection error:'));

  mongoose.set('debug', true);

  global.db = {
    mongoose: mongoose,
    //models
  
    Guide: require('./Guide')(mongoose),
    User: require('./User')(mongoose)
  };

}

module.exports = global.db;