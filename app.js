/**
 * Module dependencies.
 */


/*var allowedOrigins = '*';*/

var express = require('express');
var http = require('http');
var path = require('path');
var handlebars = require('handlebars');
var cons = require('consolidate');
require('mongodb');
var security = require('./security');

global.db = require('./models/dbconfig.js');

var app = express();


handlebars.registerHelper('breaklines', function(text) {
  text = handlebars.Utils.escapeExpression(text);
  text = text.replace(/(\r\n|\n|\r)/gm, '<br>');
  return new handlebars.SafeString(text);
});

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.engine('jade', cons.jade);
app.engine('hbs', cons.handlebars);
app.use(express.cookieParser());
app.use(express.bodyParser());
app.use(express.session({ secret: 'keyboard cat' }));
app.use(security.passport.initialize());
app.use(security.passport.session());
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", req.headers.origin); // this could be highly insecure. It´s best to whitelist the allowed Origins.
  res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
  res.header("Access-Control-Allow-Credentials", "true");
  next();
});
app.use(express.static('public'));
app.use(app.router);


// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}
require('./routes/routes')(app, security.passport);


http.createServer(app).listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});
