module.exports = function () {
  var passport = require('passport')
    , LocalStrategy = require('passport-local').Strategy;
  var crypto = require('crypto');


  passport.use(new LocalStrategy(
    function (username, password, done) {
      db.User.findOne({ username: username },{}, function (err, user) {
        if (err) {
          return done(err);
        }
        if (!user) {
          return done(null, false, { message: 'Incorrect username.' });
        }
        if (!user.validatePassword(password, crypto)) {
          return done(null, false, { message: 'Incorrect password.' });
        }
        return done(null, user);
      });
    }
  ));


  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    db.User.findById(id, function(err, user) {
      done(err, user);
    });
  });

  return{
    passport: passport
  };
}();
