'use strict';

/* App Module for LighthouseJS */

var lighthouseApp = angular.module('lighthouseApp', ['lighthouseControllers', 'lighthouseAdminControllers', 'ngRoute', 'ui.sortable']);

lighthouseApp.config(['$routeProvider', '$sceDelegateProvider', '$logProvider',
  function ($routeProvider, $sceDelegateProvider, $logProvider) {

    if (LighthouseJS.options.debug)
      $logProvider.debugEnabled(true);

    $sceDelegateProvider.resourceUrlWhitelist([
      // Allow loading from our assets domain.  Notice the difference between * and **.
      window.location.protocol + LighthouseJS.options.root + '/angular/**']);

    $routeProvider.
      when('/help', {
        templateUrl: LighthouseJS.options.root + '/angular/partials/guide-list.html',
        controller: 'LighthouseListCtrl'
      }).
      when('/administrator', {
        templateUrl: LighthouseJS.options.root + '/angular/partials/administrator.html',
        controller: 'LighthouseAdministrator'
      }).
      when('/administrator/guide/:guideId', {
        templateUrl: LighthouseJS.options.root + '/angular/partials/administrator.html',
        controller: 'LighthouseAdministrator'
      }).
      when('/hide', {
        template: '',
        controller: 'LighthouseListCtrl'
      }).
      when('/all', {
        templateUrl: LighthouseJS.options.root + '/angular/partials/guide-list.html',
        controller: 'LighthouseListCtrl'
      }).
      when('/login', {
        templateUrl: LighthouseJS.options.root + '/angular/partials/login.html',
        controller: 'LighthouseListCtrl'
      }).
      otherwise({
        redirectTo: '/help'
      })
  }

]);

lighthouseApp.factory('lighthouseJS', function ($window) {
  return $window.LighthouseJS;
});

