'use strict';
var lighthouseAdminControllers = angular.module('lighthouseAdminControllers', []);

lighthouseAdminControllers.controller('LighthouseAdministrator', ['$scope', '$http', '$route', '$routeParams', '$location', 'lighthouseJS','$rootScope',
  function ($scope, $http, $route, $routeParams, $location, lighthouseJS, $rootScope) {

    if (!$rootScope.user) {
      document.location = '#/login';
    }

    if (localStorage.guia) {
      $scope.guides = JSON.parse(localStorage.guia);
    }
    else {
      if ($routeParams.guideId) {
        $http.jsonp(lighthouseJS.options.root + '/guide/' + $routeParams.guideId + '/edit?callback=JSON_CALLBACK').success(function (data) {
          $scope.guides = data;
        });
      } else {
        $scope.currentPage = lighthouseJS.options.currentPage;

        $scope.guides =
        {
          name: "Guide name",
          description: 'guide description',
          startUrl: '/',
          nextGuides: "next guides",
          roles: ['root'],
          pages: [
            {
              name: lighthouseJS.options.currentPage,
              description: "description for page 1",
              steps: [
                {
                  type: "bubble",
                  title: "Title",
                  content: "content",
                  continueAfter: '',
                  continueWhen: 'click classname',
                  showAt: "className",
                  showSkip: true,
                  showContinue: true,
                  align: "right middle",
                  help: ""
                }
              ]
            }
          ]
        }
        ;
      }


    }

    $scope.test = function (hash) {
      $(hash).fadeOut().fadeIn();
    };

    $scope.addStep = function ($index) {

      $scope.guides.pages[$index].steps.push({
        type: "bubble",
        title: "Title",
        content: "",
        continueAfter: "5",
        continueWhen: 'click  classname',
        showAt: "classname",
        showSkip: true,
        showContinue: true,
        align: "right middle",
        help: ""
      })
    };
    $scope.removeStep = function (page, index) {
      page.steps.splice(index, 1);
    };

    $scope.addPage = function () {
      $scope.guides.pages.push({
        name: lighthouseJS.options.currentPage,
        description: "Description for another page",
        steps: [
          {
            type: "overlay",
            title: "Title",
            content: "content",
            continueAfter: 5,
            continueWhen: 'click classname',
            showAt: "className",
            showSkip: true,
            showContinue: true,
            align: "right middle",
            help: ""
          }
        ]
      })
    };
    $scope.addGuide = function () {
      $scope.guides.push({
        name: "another guide",
        description: "another guide description",
        pages: [
          {
            name: "another Page",
            description: "Description for another page",
            steps: [
              {
                type: "overlay",
                title: "Title",
                content: "content",
                continueAfter: 5,
                continueWhen: ['click', 'classname'],
                showAt: "className",
                showSkip: true,
                showContinue: true,
                align: "right middle",
                help: false
              }
            ]
          }
        ]});
    };

    $scope.remove = function ($index, guide) {
      guide.pages.splice($index, 1);
    };

    $scope.serverSave = function () {
      var url = lighthouseJS.options.root + '/guide/';
      $http({
        method: 'POST',
        url: url,
        data: $scope.guides
      }).success(function (error, data) {
          alert('data has been saved');
          console.log('success');
          console.log($scope.guides.roles);
          // $location.path('#/saved').replace();
          document.location = '#/help';
        }).error(function (error, data) {
          $scope.lastSaved = new Date();
          $scope.guides = data;
          alert(error);
        });

    };

    $scope.deleteGuide = function () {
      var url = lighthouseJS.options.root + '/guide/delete';
      $http({
        method: 'POST',
        url: url,
        data: $scope.guides
      }).success(function (error, data) {

          error ? console.log(error) : console.log(data)
        })
    }


  }]);

