'use strict';

var lighthouseControllers = angular.module('lighthouseControllers', []);

lighthouseControllers.controller('LighthouseListCtrl', ['$scope', '$http', 'lighthouseJS', '$location', '$rootScope', '$http',
  function ($scope, $http, lighthouseJS, $location, $rootScope, $httpProvider) {
    $http.defaults.withCredentials = true;
    delete $http.defaults.headers.common["X-Requested-With"];

    var role = lighthouseJS.options.role;
    var current = lighthouseJS.options.currentPage;
    if ($location.path() == '/help') {
      $http.jsonp(lighthouseJS.options.root + '/guide/list?callback=JSON_CALLBACK&current=' + current + '&role=' + role).success(function (data) {
        $scope.guides = data;
      });
    }

    else if ($location.path() == '/all') {
      $http.jsonp(lighthouseJS.options.root + '/guide/all?callback=JSON_CALLBACK').success(function (data) {
        $scope.guides = data;
      });

    }

    $scope.startGuide = function (guide) {
      guide.currentPage = lighthouseJS.options.currentPage;
      if (!this.guidelineLoaded) {
        this.guidelineLoaded = true;
        ljs.load('guideline', function () {
          var guideHelper = new GuidelineHelper(guide);
          guideHelper.start(guide);
        });
      } else {
        var guideHelper = new GuidelineHelper(guide);
        guideHelper.start(guide);
      }
    };

    $scope.goToAdmin = function () {

      if ($rootScope.user) {
        document.location = '#/administrator';
        return false;
      }

      var url = lighthouseJS.options.root + '/user/check';

      $http({
        method: 'POST',
        url   : url
      }).success(function (data, status) {
        document.location = '#/administrator';
      }).error(function (error, data) {
        document.location = '#/login';
      });

      return false;
    }

    $scope.verify = function (user) {

      var url = lighthouseJS.options.root + '/user/login';

      $http({
        method : 'POST',
        url    : url,
        data   : $.param(user),
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).success(function (error, data) {
        document.location = '#/administrator';
        $rootScope.user = user;
      }).error(function (error, data) {
        $scope.error = data;
      });


    }
  }
]);

