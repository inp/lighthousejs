[
  {"name"       : "Guide name", "description": "guide description", "currentPage": "default/indexSuccess", "pages": [
    {"name"       : "another Page", "description": "Description for another page", "steps": [
      {"name": "primer paso", "type": "overlay", "title": "title", "content": "content", "continueAfter": 5, "continueWhen": "click classname", "showAt": "className", "showSkip": true, "showContinue": true, "align": "right middle", "$$hashKey": "02D"}
    ], "$$hashKey": "02B"},
    {"name"       : "another Page", "description": "Description for another page", "steps": [
      {"name": "primer paso", "type": "overlay", "title": "title", "content": "content", "continueAfter": 5, "continueWhen": "click classname", "showAt": "className", "showSkip": true, "showContinue": true, "align": "right middle", "$$hashKey": "02H"}
    ], "$$hashKey": "02F"}
  ], "$$hashKey": "00J"}
]