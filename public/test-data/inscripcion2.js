var guide1 = new Guideline.Guide("inscripcion2");

var inscripcionDepIndividual = guide1.addPage("default/indexSuccess");

inscripcionDepIndividual.addStep(

  {
    "type"        : "bubble",
    "title"       : "menu inscripciones",
    "content"     : "despliegue el menu inscripciones",
    "continueWhen": "mouseover .node_inscripciones",
    "showAt"      : ".node_inscripciones",
    "showSkip"    : true,
    "showContinue": true,
    "align"       : "right middle"
  });
inscripcionDepIndividual.addStep({

    "type"   : "bubble",
    "title"  : "clic en inscripciones",
    "content": "haga clic en el ítem inscripción del menú inscripciones",
    "showAt" : ".node_formularioInscripcion",
    "align"  : "right middle "
  }
);


var seleccionDeporte = guide1.addPage('inscripcion/seleccionarDeporteSuccess');
seleccionDeporte.addStep(
  {
    "type"        : "bubble",
    "title"       : "Seleccione el deporte",
    "content"     : "Haga click en el deporte en el cual quiera inscribir nuevos deportistas",
    "continueWhen": "click .select-sport ul li",
    "showAt"      : ".select-sport",
    "align"       : "right middle"
  }
);


var seleccionTipo = guide1.addPage('inscripcion/seleccionarTipoSuccess');
seleccionTipo.addStep(
  {
    "type"        : "bubble",
    "title"       : "Seleccione el tipo de prueba",
    "content"     : "Haga click en tipo de prueba ",
    "continueWhen": "click .individuales.png.link",
    "showAt"      : ".tipo ul",
    "align"       : "right middle"
  }
);
var seleccionPrueba = guide1.addPage('inscripcion/filtroPruebasGrupalesSuccess');

seleccionPrueba.addStep(
  {"name"         : "click en la prueba a inscribir",
    "type"        : "bubble",
    "title"       : "seleccione la prueba",
    "content"     : "Haga clic en la prueba a la cual desea inscribir al deportista",
    "continueWhen": "click .instruction .lista li a",
    "showAt"      : ".instruction .lista li a ",
    "align"       : "right middle"}
);

var seleccionRama = guide1.addPage('inscripcion/seleccionarRamaSuccess');
seleccionRama.addStep(
  {
    "type"        : "bubble",
    "title"       : "Selecione la rama",
    "content"     : "haga clic en la rama en la que quiera inscribir al deportista",
    "continueWhen": "click .branch ul li",
    "showAt"      : ".branch",
    "align"       : "right middle"
  }

);

var seleccionantiguedad = guide1.addPage('inscripcion/filtroFormularioSuccess');
seleccionantiguedad.addStep(
  {
    "type"        : "bubble",
    "title"       : "Seleccione",
    "content"     : "seleccione si el deportista ya ha sido inscrito con anterioridad"+" <br>"+" o si desea inscribir a un deportista en nuevas pruebas",
    "continueWhen": "click .login-form",
    "continueAfter" : 10 ,
    "showAt"      : ".login-form",
    "align"       : "right middle"
  }

);
seleccionantiguedad.addStep(
  {
    "type"        : "bubble",
    "title"       : "Seleccione el deportista a inscribir en esta prueba",
    "content"     : "ingrese el nombre del deportista previemente inscrito",
    "continueWhen": "click #form-inscrito",
    "showAt"      : "#form-inscrito",
    "align"       : "right middle"
  }

);
seleccionantiguedad.addStep(
  {
    "type"        : "bubble",
    "title"       : "Selecione el deportista a inscribir en esta prueba",
    "content"     : "haga click sobre el nombre del deportista a inscribir",
    "continueWhen": "click .search-item",
    "showAt"      : ".search-item",
    "align"       : "right middle"
  }

);

var inscripcionPrueba = guide1.addPage('inscripcion/pruebasIndividualesSuccess ');
inscripcionPrueba.addStep({
  "name"         : "formulario inscripcion deportista",
  "type"        : "bubble",
  "title"       : "Seleccione la prueba",
  "content"     : "Seleccione una o varias pruebas a las que quiera inscribir al deportista",
  "continueWhen": "click #formularioPruebasIndividuales>button",
  "showAt"      : "#formularioPruebasIndividuales",
  "align"       : "right middle"

});



var personaNueva = guide1.addPage('inscripcion/mostrarFormularioSuccess');
personaNueva.addStep(
  {"name"         : "formulario inscripcion deportista",
    "type"        : "bubble",
    "title"       : "Ingrese los datos del deportista",
    "content"     : "ingrese los datos requeridos en el formulario de inscripcion de deportistas",
    "continueWhen": "click #documento",
    "showAt"      : "#formInscripcion",
    "align"       : "right middle"}

);

personaNueva.addStep(
  {"name"         : "guardado parcial",
    "type"        : "bubble",
    "title"       : "guardar por ahora",
    "content"     : "al hacer click en el botón \"Guardar por ahora\" se guardaran los datos ingresados hasta el momento pero no se inscribira al deportista en ninguna prueba hasta que los datos estén completos",
    "continueWhen": "mouseover  #botonGuardarParcial button",
    "showAt"      : "#botonGuardarParcial",
    "align"       : "right middle"}

);
personaNueva.addStep(
  {"name"         : "guardar formulario",
    "type"        : "bubble",
    "title"       : "guardar",
    "content"     : "haga click en guardar para guardar los datos ingresados y finalizar la inscripcion del deportista en la prueba seleccionada",
    "continueWhen": "click #botonGuardar button",
    "showAt"      : "#botonGuardar",
    "align"       : "right middle"}

);




guide1.register();