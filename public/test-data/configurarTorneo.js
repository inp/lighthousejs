var configurartorneo = new Guideline.Guide("configurarTorneo", ['/sistema.php/inscripcion', false]);

var menuProgramacion = configurartorneo.addPage("default/indexSuccess");

menuProgramacion.addStep(
  {"name": "menu programacion",
    "type": "bubble",
    "title": "menu programación",
    "content": "despliegue el menú programación",
    "continueWhen": "mouseover .node_programacion",
    "showAt": ".node_programacion",
    "showSkip": false,
    "showContinue": false,
    "align": "right middle",
    "$$hashKey": "00L"}
);


menuProgramacion.addStep(
  {"name": "menu torneos",
    "type": "bubble",
    "title": "torneos",
    "content": "haga clic en torneos",
    "continueWhen": "click .node_torneos",
    "showAt": ".node_torneos",
    "showSkip": false,
    "showContinue": false,
    "align": "right middle",
    "$$hashKey": "00N"}
);
var menuTorneos = configurartorneo.addPage("esquematorneo/listarSuccess");

menuTorneos.addStep(
  {"name": "seleccione la prueba",
    "type": "bubble",
    "title": "seleccione la prueba",
    "content": "seleccione la prueba",
    "continueWhen": "click #ext-gen17 .x-grid3-row, .x-grid3-row-selected",
    "showAt": "#ext-gen17 .x-grid3-row, .x-grid3-row-selected",
    "showSkip": true,
    "showContinue": true,
    "align": "right middle", "$$hashKey": "019"}
);
menuTorneos.addStep(
  {"name": "click en configurar",
    "type": "bubble",
    "title": "configurar torneo",
    "content": "haga click en configurar torneo",
    "continueWhen": "click . x-btn-text btn_wrench_orange",
    "showAt": ".btn_wrench_orange",
    "showSkip": false,
    "showContinue": false,
    "align": "right middle",
    "$$hashKey": "01B"}
);
menuTorneos.addStep(
  {"name": "click en siguiente",
    "type": "bubble",
    "title": "title",
    "content": "haga click en siguiente",
    "continueWhen": "click .x-panel-bwrap .x-toolbar-right button",
    "showAt": ".x-panel-bwrap .x-toolbar-right button",
    "showSkip": false,
    "showContinue": false,
    "align": "right middle",
    "$$hashKey": "01D"}
);
menuTorneos.addStep(
  {"name": "sistema de juego",
    "type": "bubble",
    "title": "sistema de juego",
    "content": "seleccione el sistema de juego que desea usar en el torneo",
    "continueWhen": "click .boton-torneos",
    "showAt": ".boton-torneos",
    "showSkip": true,
    "showContinue": true,
    "align": "right middle",
    "$$hashKey": "00C"}
);
menuTorneos.addStep(
  {"name": "click en siguiente",
    "type": "bubble",
    "title": "siguiente",
    "content": "haga click en siguiente",
    "continueWhen": "click .x-panel-bwrap .x-toolbar-right button",
    "showAt": ".x-panel-bwrap .x-toolbar-right button",
    "showSkip": false,
    "showContinue": false,
    "align": "right middle",
    "$$hashKey": "00E"}
);
menuTorneos.addStep(
  {"name": "division grupos",
    "type": "bubble",
    "title": "division participantes",
    "content": "seleccione como desea dividir los participantes",
    "continueWhen": "click .boton-torneos",
    "showAt": ".boton-torneos",
    "showSkip": false,
    "showContinue": false,
    "align": "right middle",
    "$$hashKey": "00G"}
);
menuTorneos.addStep(
  {"name": "click en siguiente",
    "type": "bubble",
    "title": "siguiente",
    "content": "haga click en siguiente",
    "continueWhen": "click .x-panel-bwrap .x-toolbar-right button",
    "showAt": ".x-panel-bwrap .x-toolbar-right button",
    "showSkip": false,
    "showContinue": false,
    "align": "right middle",
    "$$hashKey": "00I"}
);
menuTorneos.addStep(
  {"name": "resultados",
    "type": "bubble",
    "title": "resultados",
    "content": "defina como van a se los resultados haga click en una de las opciones",
    "continueWhen": "click .boton_torneos",
    "showAt": ".boton_torneos",
    "showSkip": false,
    "showContinue": false,
    "align": "right middle",
    "$$hashKey": "008"}
);
menuTorneos.addStep(
  {"name": "presione siguiente",
    "type": "bubble",
    "title": "siguiente",
    "content": "haga click en siguiente",
    "continueWhen": "click .x-toolbar-right button",
    "showAt": ".x-toolbar-right button",
    "showSkip": false,
    "showContinue": false,
    "align": "right middle",
    "$$hashKey": "00A"}
);
menuTorneos.addStep(
  {"name": "reglas de desempate",
    "type": "bubble",
    "title": "regals de desempate",
    "content": "seleccione cuales seran las reglas de desempate \n<br>\nseleccione la opcion deseada y luego presione la flecha hacia la derecha o izquierda según sean las reglas del torneo",
    "continueWhen": "click #x-form-el-ext-comp-1085",
    "showAt": "#x-form-el-ext-comp-1085",
    "showSkip": false,
    "showContinue": true,
    "align": "right middle",
    "$$hashKey": "00C"}
);
menuTorneos.addStep(
  {"name": "guarde los cambios",
    "type": "bubble",
    "title": "guardar",
    "content": "haga click en guardar",
    "continueWhen": "click .btn_save",
    "showAt": ".btn_save",
    "showSkip": true,
    "showContinue": true,
    "align": "right middle",
    "$$hashKey": "00E"}
);


configurartorneo.register();
