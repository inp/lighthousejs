var inscripcionApoyo = new Guideline.Guide("copiarApoyo", ['/sistema.php/inscripcion', false]);

var menuInscripcionApoyo = inscripcionApoyo.addPage("default/indexSuccess");

menuInscripcionApoyo.addStep(
  {
    "name"         : "menu inscripciones",
    "type"         : "bubble",
    "title"        : "Title",
    "content"      : "despliegue el menú inescripciones",
    "continueWhen" : "mouseover .node_inscripciones",
    "showAt"       : ".node_inscripciones",
    "showSkip"     : false,
    "showContinue" : false,
    "align"        : "right middle",
    "$$hashKey"    : "00H"
  }
);

menuInscripcionApoyo.addStep(
  {
    "name"         : "menú personal de apoyo",
    "type"         : "bubble",
    "title"        : "title",
    "content"      : "haga click en personal de apoyo",
    "continueWhen" : "click .node_personalDeApoyo",
    "showAt"       : ".node_personalDeApoyo",
    "showSkip"     : false,
    "showContinue" : false,
    "align"        : "right middle",
    "$$hashKey"    : "00K"
  }
);

var gridApoyo = inscripcionApoyo.addPage("apoyo/listarSuccess");

gridApoyo.addStep(
  {"name": "buscar apoyo existente", "type": "bubble", "title": "ingrese el nombre o documento", "content": "Escriba el nombre o la indentificación del personal de apoyo que desea buscar y selecciónelo para copiarlo al evento actual",  "continueWhen": "click .x-toolbar-left-row .x-form-field-wrap", "showAt": ".x-toolbar-left-row .x-form-field-wrap", "showSkip": true, "showContinue": true, "align": "right middle", "$$hashKey": "008"}
);

gridApoyo.addStep(
  {"name": "formulario inscripcion", "type": "bubble", "title": "llene el formulario", "content": "ingrese los datos requeridos en el formulario de inscripcion",  "continueWhen": "click #documento", "showAt": "#formularioInscripcion", "showSkip": false, "showContinue": false, "align": "right middle", "$$hashKey": "00J"}
);

gridApoyo.addStep(
  {"name": "Guardar", "type": "bubble", "title": "guarde los  datos", "content": "haga click en guadar para isncribir al personal de apoyo y finalizar elproceso",  "continueWhen": "click #botonGuardar", "showAt": "#botonGuardar", "showSkip": false, "showContinue": false, "align": "right middle", "$$hashKey": "00B"}
);

inscripcionApoyo.register();
