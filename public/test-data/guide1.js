var guide1 = new Guideline.Guide("howto");

var initialPage = guide1.addPage("initial");
/*
initialPage.addStep({
  type: "overlay",
  title: "This is lighthouse running on Hercules!",
  content: (
    "<p>Works without changing <u>anything</u> in the GitHub code</p>"+
      "<button class='btn btn-large btn-primary btn-alpha-blue hide-button'><i class='icon-circle-arrow-right'></i> Next step!</button>"+
      "<br/>"+
      "<a href='#' class='gl-skip'>I don't care. Skip this.</a>"
    ),
  overlayOptions: {
    style: {
      opacity: 0.9
    }
  }
});
*/
initialPage.addStep({
  title: "Place the mouse cursor over this menu Item",
  showAt: ".node_programacion",
  align: "right middle",
  continueAfter: 5 // seconds
});
initialPage.addStep({
  title: "click the 'escenarios' menu item",
  showAt: ".node_escenarios",
  align: "right middle",
  continueAfter: 5 // seconds
});

var secondPage = guide1.addPage("second");

secondPage.addStep({
  type: "overlay",
  title: "This is lighthouse running on Hercules!",
  content: (
    "<p>Works without changing <u>anything</u> in the GitHub code</p>"+
      "<button class='btn btn-large btn-primary btn-alpha-blue hide-button'><i class='icon-circle-arrow-right'></i> Amazing... Show me more!</button>"+
      "<br/>"+
      "<a href='#' class='gl-skip'>I don't care. Skip this.</a>"
    ),
  overlayOptions: {
    style: {
      opacity: 0.9
    }
  }
});


guide1.register();