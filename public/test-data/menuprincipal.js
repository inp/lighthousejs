var guide1 = new Guideline.Guide("menu");

var initialPage = guide1.addPage("initial");
/*
initialPage.addStep({
  type: "overlay",
  title: "This is lighthouse running on Hercules!",
  content: (
    "<p>Works without changing <u>anything</u> in the GitHub code</p>"+
      "<button class='btn btn-large btn-primary btn-alpha-blue hide-button'><i class='icon-circle-arrow-right'></i> Next step!</button>"+
      "<br/>"+
      "<a href='#' class='gl-skip'>I don't care. Skip this.</a>"
    ),
  overlayOptions: {
    style: {
      opacity: 0.9
    }
  }
});
*/
initialPage.addStep({
  title: "Menu principal",
  content: "aqui encontrara todas las opciones que  le permitiran configurar y gestionar el evento",
  showAt: "#menu-hercules",
  align: "left middle",
  continueAfter: 5 // seconds
});
initialPage.addStep({
  title: "Sistema",
  content: "aqui podra configurar las opciones generales del sistema hercules",
  showAt: ".node_sistema",
  align: "right middle",
  continueAfter: 5 // seconds
});
initialPage.addStep({
  title: "opciones de configuracion exclusiva para cada evento",
  showAt: ".node_configurarEvento",
  align: "right middle",
  continueAfter: 5 // seconds
});

var secondPage = guide1.addPage("second");

secondPage.addStep({
  type: "overlay",
  title: "This is lighthouse running on Hercules!",
  content: (
    "<p>Works without changing <u>anything</u> in the GitHub code</p>"+
      "<button class='btn btn-large btn-primary btn-alpha-blue hide-button'><i class='icon-circle-arrow-right'></i> Amazing... Show me more!</button>"+
      "<br/>"+
      "<a href='#' class='gl-skip'>I don't care. Skip this.</a>"
    ),
  overlayOptions: {
    style: {
      opacity: 0.9
    }
  }
});


guide1.register();