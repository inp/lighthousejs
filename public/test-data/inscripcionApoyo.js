var inscripcionApoyo = new Guideline.Guide("inscripcionApoyo");

var menuInscripcionApoyo = inscripcionApoyo.addPage("default/indexSuccess");

menuInscripcionApoyo.addStep(
  {
    "name"         : "menu inscripciones",
    "type"         : "bubble",
    "title"        : "menu inscripciones",
    "content"      : "despliegue el menú inescripciones",
    "continueAfter": 5,
    "continueWhen" : "click .node_inscripciones",
    "showAt"       : ".node_inscripciones",
    "showSkip"     : false,
    "showContinue" : false,
    "align"        : "right middle"
  }
);

menuInscripcionApoyo.addStep(
  {"name": "menú personal de apoyo", "type": "bubble", "title": "menu personal apoyo", "content": "haga click en personal de apoyo",  "continueWhen": "click .node_personalDeApoyo", "showAt": ".node_personalDeApoyo", "showSkip": false, "showContinue": false, "align": "right middle", "$$hashKey": "00K"}
);

var gridApoyo = inscripcionApoyo.addPage("apoyo/listarSuccess");

gridApoyo.addStep(
  {"name": "Inscribir nuevo", "type": "bubble", "title": "menu personal de apoyo", "content": "haga click en personal de apoyo",  "continueWhen": "click td em .x-btn-text.btn_create", "showAt": "td em .x-btn-text.btn_create", "showSkip": false, "showContinue": false, "align": "right middle", "$$hashKey": "00I"}
);
gridApoyo.addStep(
  {"name": "formulario inscripcion", "type": "bubble", "title": "title", "content": "ingrese los datos requeridos en el formulario de inscripcion", "continueWhen": "click #documento", "showAt": "#formularioInscripcion", "showSkip": false, "showContinue": false, "align": "right middle", "$$hashKey": "00J"}
);

gridApoyo.addStep(
  {"name": "Guardar", "type": "bubble", "title": "title", "content": "haga click en guadar para isncribir al personal de apoyo y finalizar elproceso",  "continueWhen": "click #botonGuardar", "showAt": "#botonGuardar", "showSkip": false, "showContinue": false, "align": "right middle", "$$hashKey": "00B"}
);

inscripcionApoyo.register();