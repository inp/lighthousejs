ljs
  .addAliases({
    angular: '//ajax.googleapis.com/ajax/libs/angularjs/1.2.9/angular.js',
    route: '//ajax.googleapis.com/ajax/libs/angularjs/1.2.9/angular-route.js'

  });


LighthouseJS = function () {

  var start = function () {

    var me = this;

    if (!me.started) {
      var loading = '<div id="lighthouseLoader" class="lhjs-loading">' +
        '<div id="blockG_1" class="lighthouseLoader_blockG"></div>' +
        '<div id="blockG_2" class="lighthouseLoader_blockG"></div>' +
        '<div id="blockG_3" class="lighthouseLoader_blockG"></div>' +
        '</div>';

      this.element.append(loading);
      ljs.load('angular', 'route',
        me.root + '/jquery-ui-1.11.0.custom/jquery-ui.min.js',
        me.root + '/jquery-ui-1.11.0.custom/jquery-ui.min.css',
        me.root + '/components/angular-ui-sortable/sortable.min.js',
        me.root + '/angular/js/app.js',
        me.root + '/angular/js/controllers.js',
        me.root + '/angular/js/adminControllers.js',
        function () {
          angular.element(document).ready(function () {
            me.started = true;
            angular.bootstrap(me.element, ['ngRoute', 'lighthouseApp']);
            document.getElementById('lighthouseLoader').remove();

          });

        })
    }
    location.hash = '';
  };

  this.insertIcon = function () {

    this.element = $(this.options.element);
    if (!this.element)
      console.log('Origin element has not been found: "' + options.element);
    var anchor = document.createElement('a');
    var ngView = document.createElement('div');

    this.element.append(anchor);
    ngView.setAttribute('ng-view', '');
    this.element.append(ngView);

    $(anchor)
      .addClass('help-button')
      .click($.proxy(start, this));

    anchor.innerHTML = this.options.text;

  };


  this.init = function (options) {
    var me = this;
    this.options = options;
    this.root = options.root;
    LighthouseJS.root = this.root;
    window.LighthouseJS = this;
    me.insertIcon();
    ljs.load(this.root + '/stylesheets/lighthouse.css');

    /* if the guide is not finished yet then  launch it automatically*/

    ljs.addAliases({
      'guideline': ['jQuery',
        me.root + '/javascripts/GuidelineHelper.js',
        me.root + '/components/LightShow/lightshow.jquery.min.js',
        me.root + '/components/jquery.scrollTo/jquery.scrollTo.min.js',
        me.root + '/components/Guidelinejs/src/guideline.js', //check path on CookieSet. It was forced but now there is no reason for it. Check
        me.root + '/stylesheets/guideline.css'
      ]
    });

    if (localStorage.currentGuide) {
      var guide = JSON.parse(localStorage.currentGuide);
      guide.currentPage = options.currentPage;

      ljs.load('guideline', function () {
        var helper = new GuidelineHelper(guide);
        helper.continueCurrentGuide();
      })

    }
  };


};
