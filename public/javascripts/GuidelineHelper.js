GuidelineHelper = function (guide) {

  function setGuidelinePageOffset(guidelineGuide, guide) {
    var nextPage = undefined;
    for (var pageOffset = 0; nextPage = guidelineGuide.getPage(pageOffset); pageOffset++)
    {
      if (nextPage.name == guide.currentPage)
        break;
    }
    guidelineGuide.setPageOffset(pageOffset - 1);

  }

  function removeCurrentGuide() {
    delete localStorage.currentGuide;
  }


  this.start = function () {

    ljs.load(LighthouseJS.root + '/guide/' + guide._id + '/guide', function () {

      var guidelineGuide = Guideline.getGuide(guide.name);
      guidelineGuide.restart();
      Guideline.getGuide(guide.name).start();
      setGuidelinePageOffset(guidelineGuide, guide);
      guidelineGuide.notifyPageChange(guide.currentPage);
      localStorage.currentGuide = JSON.stringify(guide);
      Guideline.getGuide(guide.name).on('complete', removeCurrentGuide);
      Guideline.getGuide(guide.name).on('skip', removeCurrentGuide);
    })
  };



  this.continueCurrentGuide = function () {
    ljs.load(LighthouseJS.root+ '/guide/' + guide._id + '/guide', function () {

      var guidelineGuide = Guideline.getGuide(guide.name);
      setGuidelinePageOffset(guidelineGuide, guide);
      guidelineGuide.notifyPageChange(guide.currentPage);
      localStorage.currentGuide = JSON.stringify(guide);
      Guideline.getGuide(guide.name).on('complete', removeCurrentGuide);
      Guideline.getGuide(guide.name).on('skip', removeCurrentGuide);
    })
  }
}
