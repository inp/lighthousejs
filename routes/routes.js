module.exports = function (app, passport) {

  var authorization = function (role) {
    return function (req, res, next) {
      console.log(req.session);
      if (req.session.user)
        next();
      else
        res.send(403);
    }
  };


  var guideList = require('../modules/guides/controllers.js');

  app.get('/guide/:id/guide', guideList.guide);
  app.get('/guide/list', guideList.list);

  app.post('/guide/', guideList.save);
  app.post('/guide/delete', authorization('admin'), guideList.postDelete);
  app.get('/guide/:id/edit', authorization('admin'), guideList.edit);
  app.get('/guide/all', authorization('admin'), guideList.all);


  var userList = require('../modules/users/controllers.js')(passport);
  app.post('/user/login', userList.login);

  app.post('/user/check', authorization('admin'), function (req, res) {
    res.json({success: true});
  });

};
