module.exports = function (passport) {

  return {
    login: function (req, res, next) {

      passport.authenticate('local', function (err, user, info) {
        if (err)
          next(err);
        else if (!user)
          res.json(info);
        else {
          req.session.user = user;
          res.json({succes: true, s: req.session});
        }

      })(req, res, next);
    }
  }
};