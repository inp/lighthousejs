exports.list = function (request, response) {
  var role = request.query.role;
  roles =role.split(',');
  var name = request.query.current;
  db.Guide.find({"pages.name": name, 'roles':{$in: roles}}, {}, function (error, docs) {
    if (error)response.json(error);
    response.jsonp(docs);
  })
};

exports.all = function (request, response) {
  db.Guide.find({}, {}, function (error, docs) {
    if (error)response.json(error);
    response.jsonp(docs);
  })
};

exports.edit = function (request, response) {
  var id = request.params.id;
  db.Guide.findOne({_id: id}, {}, function (error, docs) {
    if (error) res.json(error);
    response.jsonp(docs);
  })
};

exports.postDelete = function (request, response) {

  var guide = request.body;
  db.Guide.remove({_id: guide._id}, function (error, data) {
    error ? console.log(error) : console.log(data);
    response.json(data);
  })

}

exports.save = function (request, response) {
  var updatedGuide = request.body;
  if (updatedGuide._id) {
    var id = updatedGuide._id;
    var pages = updatedGuide.pages;
    //delete the ids so mongoose can save the embedded documents
    for (var pageIndex = 0; pageIndex < pages.length; pageIndex++) {
      for (var stepIndex = 0; stepIndex < pages[pageIndex].steps.length; stepIndex++) {
        delete pages[pageIndex].steps[stepIndex]._id;
      }
      delete pages[pageIndex]._id;
    }

    delete updatedGuide._id;

    db.Guide.findOneAndUpdate({_id: id}, updatedGuide, {upsert: true},
      function (err, guide) {
        if (err) {
          response.json(err);
        }
        else   response.json(guide);
      });
  }
  else {
    var newGuide = db.Guide(updatedGuide);
    newGuide.save(function (error, data) {
      if (error)response.json(error);
      else response.json(data);
    });
  }

};


exports.guide = function (req, res) {
  var ide = req.params.id;
  db.Guide.findOne({_id: ide}, {}, function (error, guide) {
    if (error) res.json(error);
    res.render('../views/guideview.hbs', guide);
  });

};

exports.json = function (req, res) {
  var ide = req.params.id;
  db.Guide.findOne({_id: ide}, {}, function (error, guide) {
    if (error) res.json(error);
    res.json(guide);
  });
};

