LighthouseJS

LighthouseJS is an unobstrusive guide system for web applications based on AngularJS, Express and MongoDB.
This software can be integrated into any web application giving the users in-place guides on how to use the system.
The main advantage of it is that guides are automatically discovered depending on the url the user is, the button she
wants to click or the task she's trying to do.

This project started as a help system for Hercules - Sports Management System <hercules.deportevirtual.com> and it has been
released as an open source project with the MIT License.


Copyright INP LTDA www.inpltda.com
